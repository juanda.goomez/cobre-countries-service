package com.cobre.countriesservice.application.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import com.cobre.countriesservice.infraestructure.InfraestructureException;
import com.cobre.countriesservice.infraestructure.external.CountryDTO;
import com.cobre.countriesservice.infraestructure.external.CountryNameDTO;
import com.cobre.countriesservice.infraestructure.repository.MySqlQueryLogRepository;

@SpringBootTest
@AutoConfigureMockMvc
class CountriesControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	RestTemplate restTemplate;

	@MockBean
	MySqlQueryLogRepository mySqlQueryLogRepository;

	private ResponseEntity<CountryDTO[]> responseCountries = null;

	@BeforeEach
	void setup() {
		CountryNameDTO countryNameDTO = new CountryNameDTO();
		countryNameDTO.setCommon("Colombia");
		countryNameDTO.setOfficial("official Colombia");
		CountryDTO countryDTO = new CountryDTO(countryNameDTO, "region", "Subregion");
		responseCountries = ResponseEntity.status(HttpStatus.OK).body(new CountryDTO[] { countryDTO });
	}

	@Test
	void getCountriesThenStatus200() throws Exception {
		when(restTemplate.exchange(anyString(), any(), any(), eq(CountryDTO[].class))).thenReturn(responseCountries);

		mvc.perform(get("/api/v1/countries")).andExpect(status().isOk());
	}

	@Test
	void getCountriesByRegionThenStatus200() throws Exception {
		when(restTemplate.exchange(anyString(), any(), any(), eq(CountryDTO[].class))).thenReturn(responseCountries);

		mvc.perform(get("/api/v1/countries/America")).andExpect(status().isOk());
	}

	@Test
	void getCountriesThenStatus409() throws Exception {

		when(mySqlQueryLogRepository.save(any())).thenThrow(new InfraestructureException("DB connection"));

		mvc.perform(get("/api/v1/countries")).andExpect(status().isConflict());

	}

	@Test
	void getQueryLogsThenStatus200() throws Exception {

		mvc.perform(get("/api/v1/logs")).andExpect(status().isOk());

	}

}
