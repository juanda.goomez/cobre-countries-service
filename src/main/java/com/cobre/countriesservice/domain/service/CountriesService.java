package com.cobre.countriesservice.domain.service;

import java.util.List;

import com.cobre.countriesservice.domain.Country;

public interface CountriesService {

	List<Country> getCountries(String regionName);

}
