package com.cobre.countriesservice.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cobre.countriesservice.domain.QueryLog;
import com.cobre.countriesservice.domain.repository.QueryLogRepository;

@Service
public class QueryLogServiceImpl implements QueryLogService {

	private final QueryLogRepository queryLogRepository;

	@Autowired
	public QueryLogServiceImpl(QueryLogRepository queryLogRepository) {
		this.queryLogRepository = queryLogRepository;
	}

	@Override
	public void addQueryLog(String endpoint, String requestIp) {
		QueryLog queryLog = new QueryLog(endpoint, requestIp);
		queryLogRepository.save(queryLog);
	}

	@Override
	public List<QueryLog> findAll() {
		return queryLogRepository.findAll();
	}

}
