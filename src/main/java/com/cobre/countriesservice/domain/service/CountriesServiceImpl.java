package com.cobre.countriesservice.domain.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.cobre.countriesservice.domain.Country;
import com.cobre.countriesservice.infraestructure.common.CommonConstants;
import com.cobre.countriesservice.infraestructure.external.ClientServiceCountries;
import com.cobre.countriesservice.infraestructure.mapper.MapperService;

@Service
public class CountriesServiceImpl implements CountriesService {

	private final ClientServiceCountries clientServiceCountries;
	private final MapperService mapperService;

	@Autowired
	public CountriesServiceImpl(ClientServiceCountries clientServiceCountries, MapperService mapperService) {
		this.clientServiceCountries = clientServiceCountries;
		this.mapperService = mapperService;

	}

	@Override
	@Cacheable(value = CommonConstants.DEFAULT_CACHE_COUNTRIES)
	public List<Country> getCountries(String regionName) {
		List<Country> countries = new LinkedList<>();
		clientServiceCountries.getCountries(regionName)
				.forEach(countryDTO -> countries.add(mapperService.countryDTOToCountryEntity(countryDTO)));
		return countries;
	}

}
