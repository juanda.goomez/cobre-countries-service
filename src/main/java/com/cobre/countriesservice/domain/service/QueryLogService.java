package com.cobre.countriesservice.domain.service;

import java.util.List;

import com.cobre.countriesservice.domain.QueryLog;

public interface QueryLogService {
	
	 void addQueryLog(String endpoint,String requestIp);
     List<QueryLog> findAll();

}
