package com.cobre.countriesservice.domain;

import lombok.Data;

@Data
public class Country {
	
	private String commonName;
	private String officialName;
	private String region;
	private String subregion;

}
