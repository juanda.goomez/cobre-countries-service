package com.cobre.countriesservice.domain.repository;

import java.util.List;

import com.cobre.countriesservice.domain.QueryLog;

public interface QueryLogRepository {

	List<QueryLog> findAll();

	void save(QueryLog queryLog);

}
