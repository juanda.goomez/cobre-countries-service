package com.cobre.countriesservice.domain;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class QueryLog {

	private Integer id;
	private LocalDateTime requestDate;
	private String endpoint;
	private String requestIp;

	public QueryLog(String endpoint, String requestIp) {
		super();
		this.requestDate = LocalDateTime.now();
		this.endpoint = endpoint;
		this.requestIp = requestIp;
	}

}
