package com.cobre.countriesservice.application.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.cobre.countriesservice.application.response.ExceptionResponseDTO;
import com.cobre.countriesservice.domain.DomainException;
import com.cobre.countriesservice.infraestructure.InfraestructureException;

import lombok.Generated;
import lombok.extern.slf4j.Slf4j;

@Generated
@Component
@ControllerAdvice
@Slf4j
public class ExceptionController {

	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<ExceptionResponseDTO> handleArgumentErrorGeneral(HttpServletRequest req, Exception e) {
		log.error("Error interno del servidor {}", e.toString());
		ExceptionResponseDTO response = new ExceptionResponseDTO(HttpStatus.INTERNAL_SERVER_ERROR.hashCode(),
				e.toString());
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = { InfraestructureException.class })
	protected ResponseEntity<ExceptionResponseDTO> handleArgumentErrorInfraestructure(HttpServletRequest req,
			InfraestructureException e) {
		log.error("Error de infraestructura {}", e.getMessage());
		ExceptionResponseDTO response = new ExceptionResponseDTO(HttpStatus.CONFLICT.hashCode(), e.toString());
		return new ResponseEntity<>(response, HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = { DomainException.class })
	protected ResponseEntity<ExceptionResponseDTO> handleArgumentErrorDomain(HttpServletRequest req,
			DomainException e) {
		log.error("Error de dominion {}", e.getMessage());
		ExceptionResponseDTO response = new ExceptionResponseDTO(HttpStatus.BAD_REQUEST.hashCode(), e.toString());
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

}
