package com.cobre.countriesservice.application.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ExceptionResponseDTO {

	private int status;
	private String description;

}
