package com.cobre.countriesservice.application.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cobre.countriesservice.domain.QueryLog;
import com.cobre.countriesservice.domain.service.QueryLogService;
import com.cobre.countriesservice.infraestructure.common.CommonConstants;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(CommonConstants.DEFAULT_ENDPOINT_QUERY_LOGS)
public class QueryLogController {

	private final QueryLogService queryLogService;

	@Autowired
	public QueryLogController(QueryLogService queryLogService) {
		this.queryLogService = queryLogService;
	}

	@ApiOperation(value = "Get query logs", notes = "allows to obtain the list of logs of each request in the countries api")
	@GetMapping
	public List<QueryLog> getLogs(HttpServletRequest request) {
		log.info("Get logs request");
		return queryLogService.findAll();
	}

}
