package com.cobre.countriesservice.application.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cobre.countriesservice.domain.Country;
import com.cobre.countriesservice.domain.service.CountriesService;
import com.cobre.countriesservice.domain.service.QueryLogService;
import com.cobre.countriesservice.infraestructure.common.CommonConstants;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = CommonConstants.DEFAULT_ENDPOINT_COUNTRIES)
public class CountriesController {

	private final CountriesService countriesService;

	private final QueryLogService queryLogService;

	@Autowired
	public CountriesController(CountriesService countriesService, QueryLogService queryLogService) {
		this.countriesService = countriesService;
		this.queryLogService = queryLogService;
	}

	@ApiOperation(value = "Get Countries", notes = "allows to obtain the list of countries")
	@GetMapping
	public List<Country> getCountries(HttpServletRequest request) {
		log.info("Get countries request");
		queryLogService.addQueryLog(request.getRequestURI(), getRemoteAddress(request));
		return countriesService.getCountries(null);
	}

	@ApiOperation(value = "Get Countries by region name", notes = "allows to obtain the list of countries by region name")
	@GetMapping("/{regionName}")
	public List<Country> getCountriesByRegionName(@PathVariable String regionName, HttpServletRequest request) {
		log.info("Get countries by region request");
		queryLogService.addQueryLog(request.getRequestURI(), getRemoteAddress(request));
		return countriesService.getCountries(regionName);
	}

	private String getRemoteAddress(HttpServletRequest request) {
		String remoteAddr = request.getHeader(CommonConstants.DEFAULT_HEADER_ADDRESS);
		return !StringUtils.hasText(remoteAddr) ? request.getRemoteAddr() : remoteAddr;
	}

}
