package com.cobre.countriesservice.infraestructure;

public class InfraestructureException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InfraestructureException(final String message) {
		super(message);
	}
}
