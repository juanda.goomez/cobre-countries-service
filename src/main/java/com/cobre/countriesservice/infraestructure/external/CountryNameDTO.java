package com.cobre.countriesservice.infraestructure.external;

import lombok.Data;

@Data
public class CountryNameDTO {

	private String common;
	private String official;

}
