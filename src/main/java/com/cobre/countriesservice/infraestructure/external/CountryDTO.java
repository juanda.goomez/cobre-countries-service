package com.cobre.countriesservice.infraestructure.external;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CountryDTO {
	
	private CountryNameDTO name;
	private String region;
	private String subregion;

}
