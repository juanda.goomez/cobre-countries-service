package com.cobre.countriesservice.infraestructure.external;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.cobre.countriesservice.infraestructure.InfraestructureException;

@Service
public class ClientServiceCountries {

	@Value("${spring.client.countries}")
	private String urlGetCountries;

	@Value("${spring.client.countries-by-region}")
	private String urlGetCountriesByRegionName;

	private final RestTemplate restTemplate;

	@Autowired
	public ClientServiceCountries(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public List<CountryDTO> getCountries(String regionName) {
		UriComponentsBuilder builder = regionName == null ? UriComponentsBuilder.fromHttpUrl(urlGetCountries)
				: UriComponentsBuilder.fromHttpUrl(urlGetCountriesByRegionName).path(regionName);
		try {
			ResponseEntity<CountryDTO[]> response = consumeServiceCountries(restTemplate, builder.toUriString(),
					new HttpHeaders(), HttpMethod.GET);
			return Arrays.asList(response.getBody());

		} catch (Exception e) {
			throw new InfraestructureException("Error obteniendo los paises del api cliente ".concat(e.toString()));
		}
	}

	private ResponseEntity<CountryDTO[]> consumeServiceCountries(RestTemplate restTemplate, String url,
			HttpHeaders headers, HttpMethod method) {
		return restTemplate.exchange(url, method, new HttpEntity<>(headers), CountryDTO[].class);
	}

}
