package com.cobre.countriesservice.infraestructure.mapper;

import org.springframework.stereotype.Component;

import com.cobre.countriesservice.domain.Country;
import com.cobre.countriesservice.infraestructure.external.CountryDTO;

@Component
public class MapperService {

	public Country countryDTOToCountryEntity(CountryDTO countryDTO) {
		Country country = new Country();
		country.setCommonName(countryDTO.getName().getCommon());
		country.setOfficialName(countryDTO.getName().getOfficial());
		country.setRegion(countryDTO.getRegion());
		country.setSubregion(countryDTO.getSubregion());
		return country;
	}

}
