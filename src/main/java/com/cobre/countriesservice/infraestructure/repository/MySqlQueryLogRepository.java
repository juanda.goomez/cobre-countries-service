package com.cobre.countriesservice.infraestructure.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MySqlQueryLogRepository extends CrudRepository<QueryLogEntity, Integer> {

}
