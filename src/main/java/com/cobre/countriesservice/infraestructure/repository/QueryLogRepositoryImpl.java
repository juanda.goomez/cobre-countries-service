package com.cobre.countriesservice.infraestructure.repository;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cobre.countriesservice.domain.QueryLog;
import com.cobre.countriesservice.domain.repository.QueryLogRepository;
import com.cobre.countriesservice.infraestructure.InfraestructureException;

@Service
public class QueryLogRepositoryImpl implements QueryLogRepository {

	private MySqlQueryLogRepository mySqlQueryLogRepository;

	@Autowired
	public QueryLogRepositoryImpl(MySqlQueryLogRepository mySqlQueryLogRepository) {
		this.mySqlQueryLogRepository = mySqlQueryLogRepository;
	}

	@Override
	public List<QueryLog> findAll() {
		List<QueryLog> queryLogs = new LinkedList<>();
		try {
			Iterable<QueryLogEntity> logs = this.mySqlQueryLogRepository.findAll();
			logs.forEach(queryLogEntity -> queryLogs.add(queryLogEntity.toQueryLog()));
		} catch (Exception e) {
			throw new InfraestructureException("Error obteniendo los logs de la base de datos ".concat(e.toString()));
		}
		return queryLogs;

	}

	@Override
	public void save(QueryLog queryLog) {
		try {
			mySqlQueryLogRepository.save(new QueryLogEntity(queryLog));
		} catch (Exception e) {
			throw new InfraestructureException("Error guardando el log en base de datos ".concat(e.toString()));
		}
	}

}
