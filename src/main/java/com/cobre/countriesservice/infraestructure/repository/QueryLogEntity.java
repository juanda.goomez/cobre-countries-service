package com.cobre.countriesservice.infraestructure.repository;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.cobre.countriesservice.domain.QueryLog;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
public class QueryLogEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column
	private LocalDateTime requestDate;
	@Column
	private String endpoint;
	@Column
	private String requestIp;

	public QueryLogEntity(QueryLog queryLog) {
		this.requestDate = queryLog.getRequestDate();
		this.endpoint = queryLog.getEndpoint();
		this.requestIp = queryLog.getRequestIp();
	}

	public QueryLog toQueryLog() {
		return new QueryLog(id, requestDate, endpoint, requestIp);
	}

}
