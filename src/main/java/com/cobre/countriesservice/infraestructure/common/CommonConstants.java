package com.cobre.countriesservice.infraestructure.common;

public class CommonConstants {

	private CommonConstants() {
	}

	// ENDPOINTS
	public static final String DEFAULT_ENDPOINT_COUNTRIES = "/api/v1/countries";
	public static final String DEFAULT_ENDPOINT_QUERY_LOGS = "/api/v1/logs";

	// DEFAULT
	public static final String DEFAULT_HEADER_ADDRESS = "X-FORWARDED-FOR";

	// CACHE
	public static final String DEFAULT_CACHE_COUNTRIES = "countries";
	
	//SWAGGER
	public static final String DEFAULT_BASE_PACKAGE = "com.cobre.countriesservice.application.rest";


}
