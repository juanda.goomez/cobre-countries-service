
Get countries service
Test project to obtain different countries.


Starting
Clone with https: https://gitlab.com/juanda.goomez/cobre-countries-service.git
Clone with ssh: git@gitlab.com:juanda.goomez/cobre-countries-service.git


Project structure with hexagon architecture

- application
- infraestructure
- domain


Tests Cases 
Integration tests were used to cover all the flows within the process, calls, exceptions and data return were simulated through mocks.

Deploy
The deployment was carried out with Heroku following the following steps:

- Create account in Heroku
- Login through the Heroku CLI
- Create an application in Heroku using the command "heroku create"
- Connect git with the source repository of the application in Heroku
- Create a System.properties file with the configuration of the Java version
- Perform push to the main branch in Heroku using the command "git push heroku main"

Documentation swagger:
https://serene-savannah-58550.herokuapp.com/swagger-ui.html#/

Postman Collection
https://www.getpostman.com/collections/7c907a204320247976d8



Build to
Java 11
Maven Apache Maven 3.6.3
Spring boot


